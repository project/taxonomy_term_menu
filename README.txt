Overview:
--------
This module allows adding menu entries for taxonomy terms when adding or editing terms, in the same way as they can be when adding/editing a node. This is available to anyone with administer taxonomy permissions and administer menu permissions.


Requires:
--------
 - Drupal 5.0
 - enabled taxonomy.module


Credits:
-------
 - Author: Oliver Coleman, oliver@e-geek.com.au
 - http://e-geek.com.au
 - http://enviro-geek.net
